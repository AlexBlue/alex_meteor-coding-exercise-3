NPCharacter = new Mongo.Collection('n_p_character');

NPCharacter.attachSchema(new SimpleSchema({

  title:{
    type:String
  },
  level:{
    type:Number,  
  },
  npCharacterRequestIds:{
    type:[String],
    optional: true
  }

}));