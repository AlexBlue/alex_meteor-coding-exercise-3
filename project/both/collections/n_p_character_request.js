NPCharacterRequest = new Mongo.Collection('n_p_character_request');

NPCharacterRequest.attachSchema(new SimpleSchema({

  title:{
    type:String
  },
  goal:{
    type:String
  },
  npCharacterResponseIds:{
    type:[String]
  }
  
  //https://alex-meteor-coding-exercise-3-alexblue1.c9.io/scenarios

}));