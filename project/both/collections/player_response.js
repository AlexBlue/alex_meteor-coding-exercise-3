PlayerResponse = new Mongo.Collection('player_response');

PlayerResponse.attachSchema(new SimpleSchema({

title:{
    type:String
  },
   meaning:{
    type:String
  }
  
  
}));

/*
 * Add query methods like this:
 *  PlayerResponse.findPublic = function () {
 *    return PlayerResponse.find({is_public: true});
 *  }
 */